package oo1.gui;

import javax.swing.text.html.HTML;

import oo1.model.Grupo;
import oo1.model.Usuario;

public class ActualizarGrupoComponent extends WAComponent {

	private Grupo grupo;
	private Usuario nuevoAdministrador;
	private UsuarioComponent usuarioComponent;

	public static ActualizarGrupoComponent newGrupo_UsuarioComp(Grupo unGrupo, UsuarioComponent usuarioComponent) {
		return null;
	}

	public void initialize() {
	}

	public void renderContentOn(HTML html) {
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Usuario getNuevoAdministrador() {
		return nuevoAdministrador;
	}

	public void setNuevoAdministrador(Usuario nuevoAdministrador) {
		this.nuevoAdministrador = nuevoAdministrador;
	}

	public UsuarioComponent getUsuarioComponent() {
		return usuarioComponent;
	}

	public void setUsuarioComponent(UsuarioComponent usuarioComponent) {
		this.usuarioComponent = usuarioComponent;
	}

}
