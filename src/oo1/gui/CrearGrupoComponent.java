package oo1.gui;

import java.util.Collection;

import javax.swing.text.html.HTML;

import oo1.model.Contacto;

public class CrearGrupoComponent extends WAComponent {

	private PerfilComponent perfilComponent;
	private Collection<Contacto> contactosSeleccionados;
	private String nombreGrupo, estadoGrupo;

	public void initiaize() {

	}

	public void renderContentOn(HTML html) {
	}

	public PerfilComponent getPerfilComponent() {
		return perfilComponent;
	}

	public void setPerfilComponent(PerfilComponent perfilComponent) {
		this.perfilComponent = perfilComponent;
	}

	public Collection<Contacto> getContactosSeleccionados() {
		return contactosSeleccionados;
	}

	public void setContactosSeleccionados(Collection<Contacto> contactosSeleccionados) {
		this.contactosSeleccionados = contactosSeleccionados;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public String getEstadoGrupo() {
		return estadoGrupo;
	}

	public void setEstadoGrupo(String estadoGrupo) {
		this.estadoGrupo = estadoGrupo;
	}

}
