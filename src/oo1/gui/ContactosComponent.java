package oo1.gui;

import javax.swing.text.html.HTML;

public class ContactosComponent extends WAComponent {

	private PerfilComponent perfilComponent;
	private int conversacion_usuario_seleccionado;
	private int numero_nuevo_contacto;
	private String dato_msje_actual;

	public void initialize() {

	}

	public void renderContentOn(HTML html) {

	}

	public PerfilComponent getPerfilComponent() {
		return perfilComponent;
	}

	public void setPerfilComponent(PerfilComponent perfilComponent) {
		this.perfilComponent = perfilComponent;
	}

	public int getConversacion_usuario_seleccionado() {
		return conversacion_usuario_seleccionado;
	}

	public void setConversacion_usuario_seleccionado(int conversacion_usuario_seleccionado) {
		this.conversacion_usuario_seleccionado = conversacion_usuario_seleccionado;
	}

	public int getNumero_nuevo_contacto() {
		return numero_nuevo_contacto;
	}

	public void setNumero_nuevo_contacto(int numero_nuevo_contacto) {
		this.numero_nuevo_contacto = numero_nuevo_contacto;
	}

	public String getDato_msje_actual() {
		return dato_msje_actual;
	}

	public void setDato_msje_actual(String dato_msje_actual) {
		this.dato_msje_actual = dato_msje_actual;
	}

}
