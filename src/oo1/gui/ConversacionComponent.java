package oo1.gui;

import java.util.Collection;

import javax.swing.text.html.HTML;

import oo1.model.Conversacion;

public class ConversacionComponent extends WAComponent {

	private Conversacion conversacion;
	private PerfilComponent perfilComponent;

	public static ConversacionComponent newConversacion_perfilComponent(Collection<Conversacion> conversaciones,
			PerfilComponent perfilComponent) {
		return null;
	}

	public void initiaize() {

	}

	public void renderContentOn(HTML html) {
	}

	public Conversacion getConversacion() {
		return conversacion;
	}

	public void setConversacion(Conversacion conversacion) {
		this.conversacion = conversacion;
	}

	public PerfilComponent getPerfilComponent() {
		return perfilComponent;
	}

	public void setPerfilComponent(PerfilComponent perfilComponent) {
		this.perfilComponent = perfilComponent;
	}

}
