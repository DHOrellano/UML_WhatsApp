package oo1.gui;

import javax.swing.text.html.HTML;

import oo1.model.Perfil;
import oo1.model.Usuario;

public abstract class PerfilComponent extends WAComponent {

	private Perfil perfil;
	private Usuario usuarioLogueado;

	public static PerfilComponent newPerfil() {
		return null;
		
	}
	
	public void initiaize() {

	}

	public abstract void renderOn_usuario(HTML html);

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Usuario getUsuarioLogueado() {
		return usuarioLogueado;
	}

	public void setUsuarioLogueado(Usuario usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}

}
