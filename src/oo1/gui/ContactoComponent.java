package oo1.gui;

import oo1.model.Contacto;
import oo1.model.Usuario;

public class ContactoComponent {

	private Contacto contacto;
	private Usuario usuarioLog;
	private PerfilComponent perfilComponent;

	public static ContactoComponent newContacto() {
		return null;
	}

	public static ContactoComponent newContacto_perfilComponent() {
		return null;
	}

	public void initialize() {
	}

	public Contacto getContacto() {
		return contacto;
	}

	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}

	public Usuario getUsuarioLog() {
		return usuarioLog;
	}

	public void setUsuarioLog(Usuario usuarioLog) {
		this.usuarioLog = usuarioLog;
	}

	public PerfilComponent getPerfilComponent() {
		return perfilComponent;
	}

	public void setPerfilComponent(PerfilComponent perfilComponent) {
		this.perfilComponent = perfilComponent;
	}

}
