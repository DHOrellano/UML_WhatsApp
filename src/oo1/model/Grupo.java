package oo1.model;

import java.util.Collection;

public class Grupo extends Perfil {

	private Collection<Usuario> integrantes;
	private Usuario administrador = new Usuario();

	public static Grupo crearGrupoNombre_Estado_Integrante_Admin(String nombre, String estado,
			Collection<Usuario> integrantes, Usuario admin) {
		return null;

	}

	public void initialize() {}

	
	@Override
	public boolean puedeRecibirMensaje(Usuario unUsuario) {
		return true;
	}
	
	
	@Override
	public void enviarMensaje(int conversacion_usuario_seleccionado, Mensaje unMensaje) {
		// TODO Auto-generated method stub

	}

	@Override
	public void recibirMensaje(Conversacion conversacion_actual, Mensaje unMensaje) {
		// TODO Auto-generated method stub

	}


	public Collection<Usuario> getIntegrantes() {
		return integrantes;
	}

	public void setIntegrantes(Collection<Usuario> integrantes) {
		this.integrantes = integrantes;
	}

	public Usuario getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Usuario administrador) {
		this.administrador = administrador;
	}

}
