package oo1.model;

public abstract class Perfil {

	private String nombre;
	private String estado;
	private int numero;
	private Imagen foto;

	public static Perfil newNombre_numero_estado_foto(String nombre, String estado, int numero, Imagen foto) {
		return null;
	}

	public void initialize() {

	}

	public abstract boolean puedeRecibirMensaje(Usuario unUsuario);
			
	public abstract void enviarMensaje(int conversacion_usuario_seleccionado, Mensaje unMensaje);

	public abstract void recibirMensaje(Conversacion conversacion_actual, Mensaje unMensaje);

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Imagen getFoto() {
		return foto;
	}

	public void setFoto(Imagen foto) {
		this.foto = foto;
	}

}
