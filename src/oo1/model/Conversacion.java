package oo1.model;

import java.util.Collection;

public class Conversacion {

	private Contacto destino;
	private Collection<Mensaje> mensaje;

	public static Conversacion newDestino_mensaje(Contacto contacto, Mensaje mensaje) {
		return null;
	}

	public void initialize() {

	}

	public void agregarMensaje(Mensaje mensaje) {

	}

	public Contacto getDestino() {
		return destino;
	}

	public void setDestino(Contacto destino) {
		this.destino = destino;
	}

	public Collection<Mensaje> getMensaje() {
		return mensaje;
	}

	public void setMensaje(Collection<Mensaje> mensaje) {
		this.mensaje = mensaje;
	}

}
