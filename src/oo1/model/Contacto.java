package oo1.model;

public class Contacto {

	private boolean bloqueado;
	private Perfil perfil;

	public static Contacto newPerfil(Perfil perfil) {
		return null;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

}
