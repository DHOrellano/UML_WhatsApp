package oo1.model;

import java.util.Collection;

public class Usuario extends Perfil {

	private Collection<Contacto> contactos;
	private Collection<Conversacion> conversaciones;
	private Version version;

	public void initialize() {

	}

	@Override
	public boolean puedeRecibirMensaje(Usuario usuario) {
		return true;
	}

	@Override
	public void enviarMensaje(int conversacion_usuario_seleccionado, Mensaje unMensaje) {
		// TODO Auto-generated method stub

	}

	@Override
	public void recibirMensaje(Conversacion conversacion_actual, Mensaje unMensaje) {
		// TODO Auto-generated method stub

	}

	public void eliminarContacto(Contacto contacto) {

	}

	public void eliminarConveracion(Conversacion conversacion) {

	}

	public void eliminarConversacionGrupo(int numeroGrupo) {

	}

	public void salirDelGrupo(int numeroGrupo) {

	}

	public void bloquearContacto(Usuario unUsuario) {

	}
	public void desbloquearContacto(Usuario unUsuario) {

	}

	public Collection<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(Collection<Contacto> contactos) {
		this.contactos = contactos;
	}

	public Collection<Conversacion> getConversaciones() {
		return conversaciones;
	}

	public void setConversaciones(Collection<Conversacion> conversaciones) {
		this.conversaciones = conversaciones;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

}
