package oo1.model;

import java.util.Date;

public class Mensaje {

	private Contenido contenido;
	private Usuario emisor;
	private Date fecha;

	
	public static Mensaje newEmisor_contenido(Usuario emisor, Contenido contenido){
		return new Mensaje();
	}
	
	public void initialize(){
		
	}
	
	public Contenido getContenido() {
		return contenido;
	}

	public void setContenido(Contenido contenido) {
		this.contenido = contenido;
	}

	public Usuario getEmisor() {
		return emisor;
	}

	public void setEmisor(Usuario emisor) {
		this.emisor = emisor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
