package oo1.model;

public abstract class Contenido {

	private String dato;

	public void initialize() {

	}

	public abstract String getDato();

	public void setDato(String dato) {
		this.dato = dato;
	}

}
